﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System;
using DataManagement;



public class HealthController : MonoBehaviour
{
    [SerializeField]
    private Text _quanSleep;
    [SerializeField]
    private Text _quanSatiety;
    [SerializeField]
    private Text _quanUrine;
    private PersistentData _persistentData;
    private SessionData _sessionData;
   

    private void Awake()
    {
        _persistentData = DataManager.I.persistentData;
        _sessionData = DataManager.I.sessionData;    
    }

    // Use this for initialization
    void Start()
    {
        
        _quanSleep.text = _persistentData.QuantSleep.ToString();
        _quanSatiety.text = _persistentData.QuantSatiety.ToString();
        _quanUrine.text = _persistentData.QuantUrine.ToString();
        StartCoroutine(QuantUpdateSatiety(_sessionData, 1f + TimeDelay(), _quanSatiety));
        StartCoroutine(QuantUpdateSleep(_sessionData, 3f + TimeDelay(), _quanSleep));
        StartCoroutine(QuantUpdateUrine(_sessionData, 4f + TimeDelay(), _quanUrine));
    }
    private void Update()
    {
        _quanSatiety.text = _sessionData.СurrentQuantSatiety.ToString();
        _quanSleep.text = _sessionData.СurrentQuantSleep.ToString();
        _quanUrine.text = _sessionData.СurrentQuantUrine.ToString();
    }

    IEnumerator QuantUpdateSatiety(SessionData sessionData , float delay, Text text)
    {
        while (true)
        {
            yield return new WaitForSeconds(delay);
            sessionData.СurrentQuantSatiety--;
            text.text = sessionData.СurrentQuantUrine.ToString();
        }
    }
    IEnumerator QuantUpdateSleep(SessionData sessionData, float delay, Text text)
    {
        while (true)
        {
            yield return new WaitForSeconds(delay);
            sessionData.СurrentQuantSleep--;
            text.text = sessionData.СurrentQuantSleep.ToString();
        }
    }
    IEnumerator QuantUpdateUrine(SessionData sessionData, float delay, Text text)
    {
        while (true)
        {
            yield return new WaitForSeconds(delay);
            sessionData.СurrentQuantUrine--;
            text.text = sessionData.СurrentQuantUrine.ToString();
        }
    }

    public void AddSleep()
    {
        _sessionData.СurrentQuantSleep += 8;
    }
    public void AddUrine()
    {
        _sessionData.СurrentQuantUrine += 4;
    }
    public void AddSatiety()
    {
        _sessionData.СurrentQuantSatiety+= 2;
    }

  
    private int TimeDelay()
    {
        int result = 0;
    
        switch (_persistentData.difficulty)
        {
            default:
                break;
        }
        if (_persistentData.difficulty == Levels.Easy)
        {
            result = 10;
        }
        if (_persistentData.difficulty == Levels.Middle)
        {
            result = 5;
        }
        if (_persistentData.difficulty == Levels.Hard)
        {
            result = 0;
        }
        return result;
    }
}
