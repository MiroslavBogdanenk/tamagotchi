﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Sounds : MonoBehaviour {

    public Scrollbar ScrollSound;
    public AudioSource audioSource;
   
	// Update is called once per frame
	void Update () 
    {
        audioSource.volume = ScrollSound.value;
	}
}
