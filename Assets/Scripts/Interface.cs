﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEditor;
using UnityEngine.SceneManagement;
using DataManagement;
public class Interface : MonoBehaviour
{
    [SerializeField]
    private GameObject _MainPanel;
    [SerializeField]
    private GameObject _OptionsPanel;
    [SerializeField]
    private GameObject _NewGamePanel;
    [SerializeField]
    private GameObject _CharacterPanel;
    [SerializeField]
    private GameObject _LevelDiffPanel;
    [SerializeField]
    private GameObject _SoundScroll;
    [SerializeField]
    private Text _InfoDiffLevel;
    [SerializeField]
    private Text ChoiceCharacter;
    private PersistentData _persistentData;
    private SessionData _sessionData;
    private GameObject[] _AllPanels = new GameObject[5];
    private HealthController healthController;
    public Text InfoCharacter;
    private int EnumNumb;

    public enum EnumLevels
    {
        Easy, Middle, Hard
    }
    public enum EnumCharachers
    {
        Girl, Man, Cat
    }

    private void Awake()
    {
        _persistentData = DataManager.I.persistentData;
        _sessionData = DataManager.I.sessionData;
        _AllPanels[0] = _MainPanel;
        _AllPanels[1] = _NewGamePanel;
        _AllPanels[2] = _OptionsPanel;
        _AllPanels[3] = _CharacterPanel;
        _AllPanels[4] = _LevelDiffPanel;
        _InfoDiffLevel.text = "Easy";

    }
    public void ChoiceLevelDiff(int enumObject)
    {
        switch (enumObject)
        {
            case 0:
                _InfoDiffLevel.text = "Easy";
                _persistentData.difficulty = Levels.Easy;
                break;
            case 1:
                _InfoDiffLevel.text = "Middle";
                _persistentData.difficulty = Levels.Middle;
                break;
            case 2:
                _persistentData.difficulty = Levels.Hard;
                _InfoDiffLevel.text = "Hard";
                break;
        }
    }
    public void CharacterInfo(int character)
    {
        switch (character)
        {
            case 0:
                InfoCharacter.text = "Girl";
                _persistentData.character = Charachers.Girl;
                break;
            case 1:
                InfoCharacter.text = "Man";
                _persistentData.character = Charachers.Man;
                break;
            case 2:
                InfoCharacter.text = "Cat";
                _persistentData.character = Charachers.Cat;
                break;
        }
    }


    public void ActivateCurrentPanel(GameObject panel)
    {
        for (int i = 0; i < _AllPanels.Length; i++)
        {
            if (panel == _AllPanels[i])
            {
                _AllPanels[i].SetActive(true);

            }
            else
            {
                _AllPanels[i].SetActive(false);
            }
        }

    }

    IEnumerator ChoiceChar()

    {
        ChoiceCharacter.text = "Select Character Please";
        yield return new WaitForSeconds(1.5f);
        ChoiceCharacter.text = string.Empty;
    }

    public void StartGame()
    {
        if (InfoCharacter.text != string.Empty)
        {
            DataManager.I.SavePersistentData();
            SceneManager.LoadScene(1);
        }
        else
        {
            StartCoroutine("ChoiceChar");
        }
    }
    public void OpenOptions()
    {
        ActivateCurrentPanel(_OptionsPanel);
    }
    public void OpenNGPanel()
    {
        ActivateCurrentPanel(_NewGamePanel);
    }
    public void OpenCharacter()
    {
        ActivateCurrentPanel(_CharacterPanel);
    }
    public void OpenMain()
    {
        ActivateCurrentPanel(_MainPanel);
    }
    public void OpenDiffLevel()
    {
        ActivateCurrentPanel(_LevelDiffPanel);
    }
    public void Exit()
    {
#if UNITY_EDITOR
        EditorApplication.isPlaying = false;
#else
      Application.Quit();
#endif
    }
    
}
