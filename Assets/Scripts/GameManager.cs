﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DataManagement;
using UnityEngine.UI;


public class GameManager : MonoBehaviour
{
    [SerializeField]
    private GameObject Man;
    [SerializeField]
    private GameObject Girl;
    [SerializeField]
    private GameObject Cat;
    private PersistentData _persistentData;
    private SessionData _sessionData;
    [SerializeField]
    private Text _time;
    [SerializeField]
    private Image[] images = new Image[10];
    private int color =10;
    // Use this for initialization
    private void Awake()
    {
        _persistentData = DataManager.I.persistentData;
        _sessionData = DataManager.I.sessionData;

    }
    private void Update()
    {
        CheckProgressTime();
        _time.text = Time.time.ToString("0");
    }
    private void Start()
    {
        InstantCharacter();
        LoadSessionData();
    }

    private void InstantCharacter()
    {
        Vector3 vector3 = new Vector3(0f, 0f, 0f);
        if (_persistentData.character == Charachers.Cat)
        {
            Instantiate(Cat, vector3, Quaternion.identity);
        }
        if (_persistentData.character == Charachers.Man)
        {
            Instantiate(Man, vector3, Quaternion.identity);
        }
        if (_persistentData.character == Charachers.Girl)
        {
            Instantiate(Girl, vector3, Quaternion.identity);
        }
    }
    private void LoadSessionData()
    {
        _sessionData.СurrentQuantSatiety = _persistentData.QuantSatiety;
        _sessionData.СurrentQuantSleep = _persistentData.QuantSleep;
        _sessionData.СurrentQuantUrine = _persistentData.QuantUrine;
    }
    public void SaveGameinfo()
    {
        _persistentData.QuantSatiety = _sessionData.СurrentQuantSatiety;
        _persistentData.QuantSleep = _sessionData.СurrentQuantSleep;
        _persistentData.QuantUrine = _sessionData.СurrentQuantUrine;
        DataManager.I.SavePersistentData();
    }
    private void CheckProgressTime()
    {
        switch ((int)Time.time)
        {
            case 2: images[0].color = new Color(0.3f, 1, 0, 1);
                break;
            case 4:
                images[1].color = new Color(0.3f, 1, 0, 1);
                break;
            case 6:
                images[2].color = new Color(0.3f, 1, 0, 1);
                break;
            case 8:
                images[3].color = new Color(0.3f, 1, 0, 1);
                break;
            case 10:
                images[4].color = new Color(0.3f, 1, 0, 1);
                break;
            case 12:
                images[5].color = new Color(0.3f, 1, 0, 1);
                break;
            case 14:
                images[6].color = new Color(0.3f, 1, 0, 1);
                break;
            case 16:
                images[7].color = new Color(0.3f, 1, 0, 1);
                break;
            case 17:
                images[8].color = new Color(0.3f, 1, 0, 1);
                break;
            case 20:
                images[9].color = new Color(0.3f, 1, 0, 1);
                break;

        }
        
    }
}