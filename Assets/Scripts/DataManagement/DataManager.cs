﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO;
using UnityEngine.SceneManagement;

namespace DataManagement
{
    public class DataManager
    {
        public static readonly DataManager I = new DataManager();
        private string _saveFilePath;
        private PersistentData _persistentData = new PersistentData();
        private SessionData _sessionData = new SessionData ();

        public SessionData sessionData
        {
            get { return _sessionData; }
        }

        public PersistentData persistentData
        {
            get { return _persistentData; }
        }

        private DataManager()
        {
            CaptureSaveFilePath();
            LoadPersistentData();
        }

        public void SavePersistentData()
        {
            string data = JsonUtility.ToJson(_persistentData, true);
            File.WriteAllText(_saveFilePath, data);
        }

        private void LoadPersistentData()
        {
            try
            {
                string data = File.ReadAllText(_saveFilePath);
                JsonUtility.FromJsonOverwrite(data, _persistentData);
            }
            catch
            {
                Debug.LogWarning("[DataManager] Can't deserialize persistent data from save. It will be default");
            }
        }

        private void CaptureSaveFilePath()
        {
            _saveFilePath = Application.persistentDataPath + "/SAVE.json";
        }
    }
}
