﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DataManagement;

//TODO: remove later when data management system is ready
public class DataTester : MonoBehaviour
{
    private PersistentData _persistentData;
    private SessionData _sessionData;

    private void Awake()
    {
        _persistentData = DataManager.I.persistentData;
        _sessionData = DataManager.I.sessionData;
    }

    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.C))
        {
            ChangePersistentData();
        }
    }

    private void ChangePersistentData()
    {
        DataManager.I.persistentData.character = Charachers.Girl;
        DataManager.I.persistentData.difficulty = Levels.Hard;
    }

	private void OnDestroy()
	{
		DataManager.I.SavePersistentData();
	}
}
